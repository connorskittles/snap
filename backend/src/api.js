import { cards } from '../config/cards.js';

export const shuffleCards = () => {
  let newDeck = Array.from(cards);
  let originalIndex = cards.length,
    randomIndex;

  while (originalIndex != 0) {
    randomIndex = Math.floor(Math.random() * originalIndex);
    originalIndex--;

    [newDeck[originalIndex], newDeck[randomIndex]] = [
      newDeck[randomIndex],
      newDeck[originalIndex],
    ];
  }
  return newDeck;
};

export const getRandomCard = () => {
  let number = Math.ceil(Math.random() * cards.length);
  return number.toString();
};

export const getCardById = (key) => {
  for (let card in cards)
    if (cards[card].id == key) return cards[card].id;
    else if (key >= 53 || key <= 0 || cards[card].id > key) return 'invalid';
};

export const getAllCards = () => {
  return cards;
};
