import express from 'express';
import {
  shuffleCards,
  getRandomCard,
  getCardById,
  getAllCards,
} from './src/api.js';
import cors from 'cors';

export default function () {
  const app = express();

  const dir = 'public/resources/images';

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(express.static('public'));
  app.use(cors());

  app.get('/', (_req, res) => res.send('Hello World!'));
  app.get('/shuffle', (_req, res) => res.status(200).send(shuffleCards()));
  app.get('/random', (_req, res) =>
    res.status(200).sendFile(`${getRandomCard()}.png`, { root: dir })
  );
  app.get('/cards', (_req, res) => res.status(200).send(getAllCards()));
  app.get('/card/:id', (_req, res) =>
    res.sendFile(`${getCardById(_req.params.id)}.png`, { root: dir })
  );
  app.get('*', (_req, res) =>
    res.status(404).send("Sorry, I don't think anything exists here!")
  );

  return app;
}
