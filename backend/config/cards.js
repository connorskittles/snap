export const cards = [
  { id: 1, card: 'Ace', type: 'hearts', value: 1, altValue: 11 },
  { id: 2, card: 'Ace', type: 'diamonds', value: 1, altValue: 11 },
  { id: 3, card: 'Ace', type: 'clubs', value: 1, altValue: 11 },
  { id: 4, card: 'Ace', type: 'spades', value: 1, altValue: 11 },

  { id: 5, card: '2', type: 'hearts', value: 2 },
  { id: 6, card: '2', type: 'diamonds', value: 2 },
  { id: 7, card: '2', type: 'clubs', value: 2 },
  { id: 8, card: '2', type: 'spades', value: 2 },

  { id: 9, card: '3', type: 'hearts', value: 3 },
  { id: 10, card: '3', type: 'diamonds', value: 3 },
  { id: 11, card: '3', type: 'clubs', value: 3 },
  { id: 12, card: '3', type: 'spades', value: 3 },

  { id: 13, card: '4', type: 'hearts', value: 4 },
  { id: 14, card: '4', type: 'diamonds', value: 4 },
  { id: 15, card: '4', type: 'clubs', value: 4 },
  { id: 16, card: '4', type: 'spades', value: 4 },

  { id: 17, card: '5', type: 'hearts', value: 5 },
  { id: 18, card: '5', type: 'diamonds', value: 5 },
  { id: 19, card: '5', type: 'clubs', value: 5 },
  { id: 20, card: '5', type: 'spades', value: 5 },

  { id: 21, card: '6', type: 'hearts', value: 6 },
  { id: 22, card: '6', type: 'diamonds', value: 6 },
  { id: 23, card: '6', type: 'clubs', value: 6 },
  { id: 24, card: '6', type: 'spades', value: 6 },

  { id: 25, card: '7', type: 'hearts', value: 7 },
  { id: 26, card: '7', type: 'diamonds', value: 7 },
  { id: 27, card: '7', type: 'clubs', value: 7 },
  { id: 28, card: '7', type: 'spades', value: 7 },

  { id: 29, card: '8', type: 'hearts', value: 8 },
  { id: 30, card: '8', type: 'diamonds', value: 8 },
  { id: 31, card: '8', type: 'clubs', value: 8 },
  { id: 32, card: '8', type: 'spades', value: 8 },

  { id: 33, card: '9', type: 'hearts', value: 9 },
  { id: 34, card: '9', type: 'diamonds', value: 9 },
  { id: 35, card: '9', type: 'clubs', value: 9 },
  { id: 36, card: '9', type: 'spades', value: 9 },

  { id: 37, card: '10', type: 'hearts', value: 10 },
  { id: 38, card: '10', type: 'diamonds', value: 10 },
  { id: 39, card: '10', type: 'clubs', value: 10 },
  { id: 40, card: '10', type: 'spades', value: 10 },

  { id: 41, card: 'Jack', type: 'hearts', value: 11 },
  { id: 42, card: 'Jack', type: 'diamonds', value: 11 },
  { id: 43, card: 'Jack', type: 'clubs', value: 11 },
  { id: 44, card: 'Jack', type: 'spades', value: 11 },

  { id: 45, card: 'Queen', type: 'hearts', value: 12 },
  { id: 46, card: 'Queen', type: 'diamonds', value: 12 },
  { id: 47, card: 'Queen', type: 'clubs', value: 12 },
  { id: 48, card: 'Queen', type: 'spades', value: 12 },

  { id: 49, card: 'King', type: 'hearts', value: 13 },
  { id: 50, card: 'King', type: 'diamonds', value: 13 },
  { id: 51, card: 'King', type: 'clubs', value: 13 },
  { id: 52, card: 'King', type: 'spades', value: 13 },
];
