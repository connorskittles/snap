import * as React from 'react';
import { useState } from 'react';
import GetImage from './GetImage';

let url = 'http://localhost:3001';
let endpoint = '/random';

function InputBox({ props }) {
  const [card, setCard] = useState('');
  const handleChange = (event) => {
    let value = event.target.value;
    !isNaN(+value) ? setCard(value) : setCard('Value is not a number');
  };

  return (
    <div className={props.className + 'Area'}>
      <input
        type={props.type}
        id={props.className}
        className={props.className}
        placeholder={props.text}
        onChange={handleChange}
      ></input>
      <h2>Card: {card}</h2>
      <h1>Random card from {url + endpoint}</h1>
      <GetImage
        props={{
          url: url + endpoint,
        }}
      />
    </div>
  );
}

export default InputBox;
