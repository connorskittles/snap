import React, { useEffect, useState } from 'react';

export default function Image({ props }) {
  const [img, setImg] = useState();

  const fetchImage = async () => {
    const res = await fetch(props.url);
    const imageBlob = await res.blob();
    const imageObjectURL = URL.createObjectURL(imageBlob);
    setImg(imageObjectURL);
  };

  useEffect(() => {
    fetchImage();
  }, []);

  return (
    <div className='imageArea'>
      <img src={img} alt='icons' />
    </div>
  );
}
