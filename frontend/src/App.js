import './App.css';
import InputBox from './components/InputBox';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <form>
          <InputBox
            props={{
              text: 'Pick a card number from 1 - 52',
              className: 'cardSelectionInput',
              type: 'number',
              functionName: 'getCard',
            }}
          />
        </form>
      </header>
    </div>
  );
}

export default App;
