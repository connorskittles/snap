# Future Snap Project

## What is it?

This repository is currently a very early stage of what I plan to be a free browser-based game of the card game "SNAP".

Currently the repo contains a frontend made from ReactJS and a backend made from ExpressJS. The two apps currently do not communicate with one another, but can be run alongside one another.

## Dependencies

- Running without Docker
  - NodeJS (I used 20.2.0)
  - NPM (I used 9.6.7)
- Running with Docker
  - Docker

## Running the App

### No Docker Solution

- Both the backend and the frontend can be started using the same commands, you just need to be in the right directory for the application you are trying to start. CD into the /backend or /frontend directory and run the following commands:

```bash
npm i &&\
npm start
```

### Docker Solution

- Using Docker you can run the app in two ways. You can either build the Docker images yourself, or you can pull them down from Dockerhub. I will list both methods below, but you only need to do one of them. In order to run the images, check the next section of this documentation.

#### Building Docker Images Yourself

Run the following commands one at a time from the root of the directory in order to build the Docker images for yourself

```bash
cd backend/
docker build -t valatore/snapapp-backend:0.0.2 .
```

```bash
cd frontend/
docker build -t valatore/snapapp-frontend:0.0.2 .
```

#### Pulling Docker Images from Dockerhub

Run the following commands to pull the Docker images from Dockerhub:

```bash
docker pull valatore/snapapp-backend:0.0.2
docker pull valatore/snapapp-frontend:0.0.2
```

#### Running the Docker Images

To run the front end and the backend together, run the docker-compose file in the root of the directory by running the following command:

```bash
docker-compose up -d
```

If you would like to close these containers, run the following command

```bash
docker-compose down -v
```

## Testing the App

### Testing the API Endpoints

- Test get all cards

```bash
curl -X GET localhost:3001/cards
```

- Get a random card

```bash
curl -X GET localhost:3001/random
```

- Test get card by ID (accepted numbers are between 1 and 52)

```bash
curl -X GET localhost:3001/card/21
```

- Test the shuffle feature

```bash
curl -X GET localhost:3001/shuffle
```

### Testing the codebase itself

Currently only the backend has tests, but they can be run from the root directory with the following commands:

```bash
cd backend/
npm test
```
